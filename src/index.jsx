import ForgeUI, {
  render,
  Fragment,
  Macro,
  Text,
  useState,
  Form,
  RadioGroup,
  Radio,
  TextArea,
  SectionMessage,
  Table,
  Head,
  Row,
  Cell,
} from "@forge/ui";

const App = () => {
  const [formState, setFormState] = useState(undefined);

  const [occurrences, setOccurrencesState] = useState([]);

  const onSubmit = async (formData) => {
    let localOccurrences = [];
    let words = formData.counterInput.split(" ");
    for (let word of words) {
      let occurrence = localOccurrences.find((x) => x.word === word);
      if (occurrence) {
        occurrence.count++;
      } else {
        localOccurrences.push({
          word: word,
          count: 1,
        });
      }
    }

    let isSortByAsc = formData.sortOrderBy === "asc" ? -1 : 1;
    localOccurrences.sort((a, b) =>
      a[formData.sortBy] < b[formData.sortBy] ? isSortByAsc : isSortByAsc * -1
    );

    setOccurrencesState(localOccurrences);
    setFormState(formData);
  };

  return (
    <Fragment>
      <Form onSubmit={onSubmit} submitButtonText="Let's start counting!">
        <TextArea
          name="counterInput"
          label="Your input field"
          isRequired="true"
        />
        <RadioGroup name="sortBy" label="Sort results by">
          <Radio defaultChecked value="word" label="Word" />
          <Radio value="count" label="Count" />
        </RadioGroup>
        <RadioGroup name="sortOrderBy" label="Sort order by">
          <Radio defaultChecked value="asc" label="Ascending" />
          <Radio value="desc" label="Descending" />
        </RadioGroup>
      </Form>
      {formState && (
        <SectionMessage appearance="confirmation">
          <Text>You entered:</Text>
          <Text>{formState.counterInput}</Text>
        </SectionMessage>
      )}
      {occurrences.length > 0 && (
        <Table>
          <Head>
            <Cell>
              <Text>Word</Text>
            </Cell>
            <Cell>
              <Text>Count</Text>
            </Cell>
          </Head>
          {occurrences.map((word) => (
            <Row>
              <Cell>
                <Text>{word.word}</Text>
              </Cell>
              <Cell>
                <Text>{word.count}</Text>
              </Cell>
            </Row>
          ))}
        </Table>
      )}
    </Fragment>
  );
};

export const run = render(<Macro app={<App />} />);
